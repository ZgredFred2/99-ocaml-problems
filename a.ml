let rec last list =
  match list with
  | [] -> None
  | [ e ] -> Some e
  | h :: t -> last t
;;

let () = assert (last [ "a"; "b"; "c"; "d" ] = Some "d")

let rec last_two list =
  match list with
  | [] -> None
  | [ a; b ] -> Some (a, b)
  | _ :: t -> last_two t
;;

let () = assert (last_two [ "a"; "b"; "c"; "d" ] = Some ("c", "d"))
let () = assert (last_two [ "a" ] = None)

let rec nth list n =
  match list with
  | [] -> None
  | h :: t -> if n = 0 then Some h else nth t (n - 1)
;;

let () = assert (nth [ "a"; "b"; "c"; "d"; "e" ] 2 = Some "c")
let () = assert (nth [ "a" ] 2 = None)

let rec length list =
  let rec lengthAux list acc =
    match list with
    | [] -> acc
    | h :: t -> lengthAux t (acc + 1)
  in
  lengthAux list 0
;;

let () = assert (length [ "a"; "b"; "c" ] = 3)
let () = assert (length [] = 0)

let rec rev list =
  match list with
  | [] -> []
  | h :: t -> rev t @ [ h ]
;;

let () = assert (rev [ 1; 2; 3; 4 ] = [ 4; 3; 2; 1 ])
let () = assert (rev [] = [])
let is_palindrome word = word = rev word
let () = assert (is_palindrome [ "x"; "a"; "m"; "a"; "x" ])
let () = assert (not (is_palindrome [ "a"; "b" ]))

type 'a node =
  | One of 'a
  | Many of 'a node list

let rec flatten list =
  let rec aux list acc =
    match list with
    | [] -> acc
    | One h :: t -> aux t (h :: acc)
    | Many h :: t -> aux t (aux h acc)
  in
  List.rev (aux list [])
;;

(* match list with *)
(* | [] -> [] *)
(* | One h :: t -> h :: flatten t *)
(* | Many h :: t -> flatten h @ flatten t *)
(* ;; *)

let f = flatten [ One "a"; Many [ One "b"; Many [ One "c"; One "d" ]; One "e" ] ]
let () = assert (f = [ "a"; "b"; "c"; "d"; "e" ])

let rec compress = function
  | a :: (b :: _ as t) -> if a = b then compress t else a :: compress t
  | smaller -> smaller
;;

let () =
  assert (
    compress [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ]
    = [ "a"; "b"; "c"; "a"; "d"; "e" ])
;;

let rec pack list =
  match list with
  | a :: (b :: _ as t) ->
    let res = pack t in
    if a = b then (a :: List.hd res) :: List.tl res else [ a ] :: res
  | [] -> []
  | [ a ] -> [ [ a ] ]
;;

let () =
  assert (
    pack [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "d"; "e"; "e"; "e"; "e" ]
    = [ [ "a"; "a"; "a"; "a" ]
      ; [ "b" ]
      ; [ "c"; "c" ]
      ; [ "a"; "a" ]
      ; [ "d"; "d" ]
      ; [ "e"; "e"; "e"; "e" ]
      ])
;;

(* let rec encode list = *)
(*   match list with *)
(*   | a :: (b :: _ as t) -> *)
(*     let res = encode t in *)
(*     let count, elem = List.hd res in *)
(*     if a = b then (count + 1, elem) :: List.tl res else (1, a) :: res *)
(*   | [ a ] -> [ 1, a ] *)
(*   | [] -> [] *)
(* ;; *)
(* let () = *)
(*   assert ( *)
(*     encode [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ] *)
(*     = [ 4, "a"; 1, "b"; 2, "c"; 2, "a"; 1, "d"; 4, "e" ]) *)
(* ;; *)
type 'a rle =
  | One of 'a
  | Many of int * 'a

let rec encode list =
  match list with
  | a :: (b :: _ as t) ->
    let res = encode t in
    let count, elem =
      match List.hd res with
      | One a -> 1, a
      | Many (count, elem) -> count, elem
    in
    if a = b then Many (count + 1, elem) :: List.tl res else One a :: res
  | [ a ] -> [ One a ]
  | [] -> []
;;

let () =
  assert (
    encode [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ]
    = [ Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e") ])
;;

let rec decode list =
  let reduceByOne e =
    let count, e = e in
    let restCount = count - 1 in
    if restCount = 1 then One e else Many (restCount, e)
  in
  match list with
  | One elem :: t -> elem :: decode t
  | Many (count, elem) :: t ->
    let restElement = reduceByOne (count, elem) in
    elem :: decode (restElement :: t)
  | [] -> []
;;

let () =
  assert (
    decode
      [ Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e") ]
    = [ "a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e" ])
;;

let rec duplicate list =
  match list with
  | h :: t -> h :: h :: duplicate t
  | [] -> []
;;

let () =
  assert (
    duplicate [ "a"; "b"; "c"; "c"; "d" ]
    = [ "a"; "a"; "b"; "b"; "c"; "c"; "c"; "c"; "d"; "d" ])
;;

let replicate list times =
  let rec replicateElement e times =
    if times = 0 then [] else e :: replicateElement e (times - 1)
  in
  let rec aux list times =
    match list with
    | h :: t -> replicateElement h times @ aux t times
    | [] -> []
  in
  aux list times
;;

let () =
  assert (replicate [ "a"; "b"; "c" ] 3 = [ "a"; "a"; "a"; "b"; "b"; "b"; "c"; "c"; "c" ])
;;

let rec drop list count =
  let rec aux list n =
    match list with
    | [] -> []
    | h :: t -> if n = 1 then aux t count else h :: aux t (n - 1)
  in
  aux list count
;;

let () =
  assert (
    drop [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 3
    = [ "a"; "b"; "d"; "e"; "g"; "h"; "j" ])
;;

(* Split a list into two parts; the length of the first part is given. *)
(* If the length of the first part is longer than the entire list, then the first part is the list and the second part is empty. *)
let rec split list n =
  let rec aux list n acc =
    match list with
    | [] -> acc, []
    | h :: t -> if n = 0 then acc, list else aux t (n - 1) (h :: acc)
  in
  let f, s = aux list n [] in
  List.rev f, s
;;

let () =
  assert (
    split [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 3
    = ([ "a"; "b"; "c" ], [ "d"; "e"; "f"; "g"; "h"; "i"; "j" ]))
;;

let () = assert (split [ "a"; "b"; "c"; "d" ] 5 = ([ "a"; "b"; "c"; "d" ], []))

let rec slice list start endd =
  let rec aux list start endd acc =
    if endd = -1
    then acc
    else (
      match list with
      | [] -> acc
      | h :: t -> aux t (start - 1) (endd - 1) (if start <= 0 then h :: acc else acc))
  in
  List.rev (aux list start endd [])
;;

let () =
  assert (
    slice [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h"; "i"; "j" ] 2 6
    = [ "c"; "d"; "e"; "f"; "g" ])
;;

let rotate list n =
  let len = List.length list in
  let n = if len = 0 then 0 else n mod len in
  let f, s = split list n in
  s @ f
;;

let () =
  assert (
    rotate [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" ] 3
    = [ "d"; "e"; "f"; "g"; "h"; "a"; "b"; "c" ])
;;

let remove_at i list =
  let a, b = split list i in
  a
  @
  match b with
  | [] -> []
  | h :: t -> t
;;

let () = assert (remove_at 1 [ "a"; "b"; "c"; "d" ] = [ "a"; "c"; "d" ])

let rec insert_at elem i list =
  let a, b = split list i in
  a @ (elem :: b)
;;

let () =
  assert (insert_at "alfa" 1 [ "a"; "b"; "c"; "d" ] = [ "a"; "alfa"; "b"; "c"; "d" ])
;;

let rec range start endd = if endd < start then [] else start :: range (start + 1) endd
let () = assert (range 4 9 = [ 4; 5; 6; 7; 8; 9 ])

(** Return elements of [list] that positions are included in [index_list]
    The first element (head of the list) is at position 0.
    [index_list] has to be sorted.
    [index_list] can include positions that don't exist in [list]
 *)
let rec take_indexes list index_list =
  let rec aux list index_list current_pos acc =
    match index_list with
    | [] -> acc
    | index_head :: index_tail ->
      (match list with
       | [] -> acc
       | list_head :: list_tail ->
         if current_pos = index_head
         then
           (* include head and go further in index_list *)
           aux list_tail index_tail (current_pos + 1) (list_head :: acc)
         else (* go further in list *)
           aux list_tail index_list (current_pos + 1) acc)
  in
  let aux_res = aux list index_list 0 [] in
  List.rev aux_res
;;

let () =
  assert (
    take_indexes [ 0; 1; 2; 3; 4; 5; 6; 7; 8; 9 ] [ 0; 3; 4; 6; 8 ] = [ 0; 3; 4; 6; 8 ])
;;

let rand_select list n =
  let rec extract list n =
    match list with
    | [] -> raise Not_found
    | h :: t ->
      if n = 0
      then h, t
      else (
        let selected, rest = extract t (n - 1) in
        selected, h :: rest)
  in
  let extract_random list max_range =
    let selected_index = Random.int max_range in
    extract list selected_index
  in
  let rec rand_select_aux list n list_len acc =
    if list_len = 0 || n = 0
    then acc
    else (
      let s, rest = extract_random list list_len in
      rand_select_aux rest (n - 1) (list_len - 1) (s :: acc))
  in
  rand_select_aux list n (List.length list) []
;;

(* requre not initializing Random module *)
let () =
  assert (rand_select [ "a"; "b"; "c"; "d"; "e"; "f"; "g"; "h" ] 3 = [ "g"; "d"; "a" ])
;;

let rec lotto_select n m = rand_select (range 1 m) n

(* gives different results than ocaml.org/problems *)
(* let () = assert (lotto_select 6 49 = [ 20; 28; 45; 16; 24; 38 ]) *)

let rec permutation list = rand_select list (List.length list)

(* let () = *)
(*   assert (permutation [ "a"; "b"; "c"; "d"; "e"; "f" ] = [ "c"; "d"; "f"; "e"; "b"; "a" ]) *)
(* ;; *)

let extract n list =
  let rec extract_aux n list current_stack =
    if n = 0 (* reving only to satisfy test *)
    then [ List.rev current_stack ]
    else (
      match list with
      | [] -> []
      (* posibilities with taking h + posibilities without taking h*)
      | h :: t ->
        extract_aux (n - 1) t (h :: current_stack) @ extract_aux n t current_stack)
  in
  extract_aux n list []
;;

let max_num = List.fold_left max 0;;

max_num [ 1; 2; 3; 4; 5; 6 ]

let () =
  assert (
    extract 2 [ "a"; "b"; "c"; "d" ]
    = [ [ "a"; "b" ]
      ; [ "a"; "c" ]
      ; [ "a"; "d" ]
      ; [ "b"; "c" ]
      ; [ "b"; "d" ]
      ; [ "c"; "d" ]
      ])
;;
