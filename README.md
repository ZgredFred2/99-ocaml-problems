# 99-ocaml-problems

My approach to [99 ocaml problems](https://ocaml.org/problems)  
Note: Most of the exercises are not optimized for tail recursion because my priority is currently on finding a solution to the problem rather than optimizing the method of solution for memory stack usage.
